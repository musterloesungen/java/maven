### Maven-Projektaufgabe

#### Ziel
Erstellen Sie ein Maven-basiertes Java-Projekt, das eine einfache Funktionalität implementiert, externe Abhängigkeiten nutzt, getestet wird und bereit für den Build und das Deployment ist.

#### Aufgabenbeschreibung

##### Schritt 1: Initialisieren des Projekts

1. **Projekt Scaffold erstellen**:
   - Erstellen Sie ein neues Maven-Projekt mit dem Archetype `maven-archetype-quickstart`.
   - Benutzen Sie das folgende Kommando, um das Projekt zu generieren:
     ```bash
     mvn archetype:generate -DgroupId=com.example -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
     ```
   - Navigieren Sie in das erstellte Projektverzeichnis:
     ```bash
     cd my-app
     ```

##### Schritt 2: Implementieren der Funktionalität

2. **Einfacher Taschenrechner**:
   - Implementieren Sie eine Klasse `Calculator` im Paket `com.example` mit den folgenden Methoden:
     ```java
     public class Calculator {
         public int add(int a, int b) {
             return a + b;
         }
         
         public int subtract(int a, int b) {
             return a - b;
         }
         
         public int multiply(int a, int b) {
             return a * b;
         }
         
         public double divide(int a, int b) {
             if (b == 0) {
                 throw new IllegalArgumentException("Division by zero is not allowed.");
             }
             return (double) a / b;
         }
     }
     ```

##### Schritt 3: Hinzufügen externer Abhängigkeiten

3. **Hinzufügen von Abhängigkeiten**:
   - Fügen Sie die folgenden Abhängigkeiten zu Ihrer `pom.xml` hinzu:
     ```xml
     <dependencies>
         <dependency>
             <groupId>junit</groupId>
             <artifactId>junit</artifactId>
             <version>4.13.2</version>
             <scope>test</scope>
         </dependency>
         <dependency>
             <groupId>org.apache.commons</groupId>
             <artifactId>commons-lang3</artifactId>
             <version>3.12.0</version>
         </dependency>
     </dependencies>
     ```

##### Schritt 4: Testen der Applikation

4. **Unit Tests erstellen**:
   - Erstellen Sie eine Testklasse `CalculatorTest` im Verzeichnis `src/test/java/com/example`:
     ```java
     import org.junit.Test;
     import static org.junit.Assert.*;

     public class CalculatorTest {

         @Test
         public void testAdd() {
             Calculator calculator = new Calculator();
             assertEquals(5, calculator.add(2, 3));
         }

         @Test
         public void testSubtract() {
             Calculator calculator = new Calculator();
             assertEquals(1, calculator.subtract(3, 2));
         }

         @Test
         public void testMultiply() {
             Calculator calculator = new Calculator();
             assertEquals(6, calculator.multiply(2, 3));
         }

         @Test
         public void testDivide() {
             Calculator calculator = new Calculator();
             assertEquals(2.0, calculator.divide(4, 2), 0.001);
         }

         @Test(expected = IllegalArgumentException.class)
         public void testDivideByZero() {
             Calculator calculator = new Calculator();
             calculator.divide(4, 0);
         }
     }
     ```

##### Schritt 5: Projekt bauen und testen

5. **Projekt bauen**:
   - Führen Sie das folgende Kommando aus, um das Projekt zu bauen und die Tests auszuführen:
     ```bash
     mvn clean install
     ```

##### Schritt 6: Deployment vorbereiten

6. **Deployment**:
   - Stellen Sie sicher, dass das erzeugte JAR-File im Verzeichnis `target` liegt.
   - Um das Projekt zu deployen, können Sie beispielsweise ein lokales Repository konfigurieren oder ein Remote-Repository verwenden.
   - Fügen Sie folgendes zu Ihrer `pom.xml` hinzu, um das JAR in ein Remote-Repository zu deployen:
     ```xml
     <distributionManagement>
         <repository>
             <id>my-repo</id>
             <url>http://your-repository-url</url>
         </repository>
     </distributionManagement>
     ```
   - Führen Sie dann den folgenden Befehl aus, um das Projekt zu deployen:
     ```bash
     mvn deploy
     ```

### Abschluss

Nach Abschluss dieser Schritte haben Sie ein funktionierendes Maven-Projekt erstellt, das eine einfache Funktionalität bietet, externe Abhängigkeiten verwendet, getestet und bereit für den Build und das Deployment ist.