### Musterlösung für die Maven-Projektaufgabe

#### Ziel
Erstellen und konfigurieren Sie ein Maven-Projekt, das eine einfache Taschenrechnerfunktionalität implementiert, externe Abhängigkeiten nutzt, getestet wird, ausführbar ist und bereit für den Build und das Deployment ist.

### Schritt 1: Initialisieren des Projekts

1. **Projekt Scaffold erstellen**:
   - Verwenden Sie das Maven Archetype Plugin, um ein neues Projekt zu generieren:
     ```bash
     mvn archetype:generate -DgroupId=com.example -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
     ```
   - Dies erstellt eine Verzeichnisstruktur mit den Standard-Maven-Verzeichnissen und einer einfachen Beispielklasse sowie einer `pom.xml`-Datei.

2. **Navigieren Sie in das Projektverzeichnis**:
   ```bash
   cd my-app
   ```

### Schritt 2: Implementieren der Funktionalität

1. **Erstellen der Klasse `Calculator`**:
   - Erstellen Sie eine neue Java-Klasse `Calculator` im Verzeichnis `src/main/java/com/example`:
     ```java
     package com.example;

     public class Calculator {
         public int add(int a, int b) {
             return a + b;
         }
         
         public int subtract(int a, int b) {
             return a - b;
         }
         
         public int multiply(int a, int b) {
             return a * b;
         }
         
         public double divide(int a, int b) {
             if (b == 0) {
                 throw new IllegalArgumentException("Division by zero is not allowed.");
             }
             return (double) a / b;
         }
     }
     ```

2. **Erstellen einer `App`-Klasse**:
   - Erstellen Sie eine `App`-Klasse im Verzeichnis `src/main/java/com/example`, um die Anwendung ausführbar zu machen:
     ```java
     package com.example;

     public class App {
         public static void main(String[] args) {
             Calculator calculator = new Calculator();
             System.out.println("Addition: " + calculator.add(5, 3));
             System.out.println("Subtraction: " + calculator.subtract(5, 3));
             System.out.println("Multiplication: " + calculator.multiply(5, 3));
             System.out.println("Division: " + calculator.divide(5, 3));
         }
     }
     ```

### Schritt 3: Hinzufügen externer Abhängigkeiten

1. **Bearbeiten der `pom.xml`**:
   - Fügen Sie die folgenden Abhängigkeiten in Ihrer `pom.xml` hinzu:
     ```xml
     <project xmlns="http://maven.apache.org/POM/4.0.0"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
         <modelVersion>4.0.0</modelVersion>
         <groupId>com.example</groupId>
         <artifactId>my-app</artifactId>
         <version>1.0-SNAPSHOT</version>
         
         <dependencies>
             <dependency>
                 <groupId>junit</groupId>
                 <artifactId>junit</artifactId>
                 <version>4.13.2</version>
                 <scope>test</scope>
             </dependency>
             <dependency>
                 <groupId>org.apache.commons</groupId>
                 <artifactId>commons-lang3</artifactId>
                 <version>3.12.0</version>
             </dependency>
         </dependencies>

         <build>
             <plugins>
                 <plugin>
                     <groupId>org.apache.maven.plugins</groupId>
                     <artifactId>maven-jar-plugin</artifactId>
                     <version>3.2.0</version>
                     <configuration>
                         <archive>
                             <manifest>
                                 <addClasspath>true</addClasspath>
                                 <classpathPrefix>lib/</classpathPrefix>
                                 <mainClass>com.example.App</mainClass>
                             </manifest>
                         </archive>
                     </configuration>
                 </plugin>
             </plugins>
         </build>
     </project>
     ```

### Schritt 4: Testen der Applikation

1. **Erstellen der Testklasse `CalculatorTest`**:
   - Erstellen Sie eine neue Testklasse `CalculatorTest` im Verzeichnis `src/test/java/com/example`:
     ```java
     package com.example;

     import org.junit.Test;
     import static org.junit.Assert.*;

     public class CalculatorTest {

         @Test
         public void testAdd() {
             Calculator calculator = new Calculator();
             assertEquals(8, calculator.add(5, 3));
         }

         @Test
         public void testSubtract() {
             Calculator calculator = new Calculator();
             assertEquals(2, calculator.subtract(5, 3));
         }

         @Test
         public void testMultiply() {
             Calculator calculator = new Calculator();
             assertEquals(15, calculator.multiply(5, 3));
         }

         @Test
         public void testDivide() {
             Calculator calculator = new Calculator();
             assertEquals(1.6667, calculator.divide(5, 3), 0.0001);
         }

         @Test(expected = IllegalArgumentException.class)
         public void testDivideByZero() {
             Calculator calculator = new Calculator();
             calculator.divide(5, 0);
         }
     }
     ```

### Schritt 5: Projekt bauen und testen

1. **Projekt bauen und testen**:
   - Führen Sie das folgende Kommando aus, um das Projekt zu bauen und die Tests auszuführen:
     ```bash
     mvn clean install
     ```
   - Dieses Kommando führt mehrere Schritte durch:
     - `clean`: Löscht das `target`-Verzeichnis, um eine saubere Umgebung zu gewährleisten.
     - `install`: Kompiliert den Code, führt die Tests aus und installiert das generierte Artefakt (JAR) in das lokale Maven-Repository.

### Schritt 6: Deployment vorbereiten

1. **Deployment konfigurieren**:
   - Fügen Sie eine `distributionManagement`-Sektion zur `pom.xml` hinzu, um das Deployment in ein Remote-Repository zu konfigurieren:
     ```xml
     <distributionManagement>
         <repository>
             <id>my-repo</id>
             <url>http://your-repository-url</url>
         </repository>
     </distributionManagement>
     ```

2. **Projekt deployen**:
   - Führen Sie das folgende Kommando aus, um das Projekt in das konfigurierte Remote-Repository zu deployen:
     ```bash
     mvn deploy
     ```
   - Dieses Kommando lädt das erzeugte JAR-File und die zugehörigen Metadaten in das angegebene Remote-Repository hoch.

### Abschluss

Diese Schritte stellen sicher, dass Sie ein funktionierendes Maven-Projekt erstellt haben, das eine grundlegende Taschenrechnerfunktionalität implementiert, externe Abhängigkeiten nutzt, umfassend getestet wird, ausführbar ist und bereit für den Build und das Deployment ist. Die Verwendung von Maven ermöglicht es, den Build-Prozess zu automatisieren und die Verwaltung von Abhängigkeiten zu vereinfachen.